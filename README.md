# Technical-Test-Life-Is-The-Game

Prueba técnica para Life is The Game.



Se ha desarrollado en la versión de Unity 2021.3.17f1 LTS.



Se ha usado el New Input System de Unity para desarrollar la solución.



En la primera parte de la prueba se ha usado el patrón de diseño de software Observer, en donde cada clic a un botón del UI notifica al observador del cambio realizado para actualizar la animación que debe de ejecutar.

Se hace uso de script PlayerData con la función DontDestroyOnLoad() para almacenar la información de la animación; esta es info es leída en la siguiente escena al ser cargada.



En la segunda parte se crea la vista en primera persona, y se hace uso de dos cámaras, una para la vista del jugador y otra para desplegar la animación seleccionada en el canvas.

Se crea una clase base Bullet de la cual heredan 3 clases adicionales para la creación de cada tipo de proyectil (parabólico, magnético y sinusoidal). Además, se crea el SO para almacenar la info correspondiente a cada tipo de proyectil, la cual es cargada en la

clase Weapon.



Para seleccionar un arma se debe de apuntar hacia ella y presionar la tecla E. Con clic izquierdo se dispara el proyectil.



El código cuenta con summaries que explican cada bloque de código.