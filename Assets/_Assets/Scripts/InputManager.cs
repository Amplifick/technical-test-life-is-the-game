using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DG
{
    public class InputManager : MonoBehaviour
    {
        [Header("References")]
        private PlayerInput _input;

        [Header("Settings")]
        public Vector2 mouseLook;
        public bool interactInput;
        public bool shootInput;

        #region Unity Functions

        private void Awake()
        {
            _input = new PlayerInput();
        }

        private void OnEnable()
        {
            _input.Enable();
        }

        private void OnDisable()
        {
            _input.Disable();
        }

        private void Update()
        {
            ReadInput();
        }

        #endregion

        /// <summary>
        /// Reads the value of each input in the Input Action Map and assigns it to the variable.
        /// </summary>
        private void ReadInput()
        {
            mouseLook = _input.Player.Camera.ReadValue<Vector2>();

            interactInput = _input.Player.Interact.triggered;
            shootInput = _input.Player.Shoot.triggered;
        }

    }
}
