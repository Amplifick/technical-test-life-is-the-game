using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class PlayerData : MonoBehaviour
    {
        [Header("Data to Save")]
        [SerializeField] private string animationSelected;

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void SaveAnimationData(string animation)
        {
            animationSelected = animation;
        }

        public void LoadAnimationData(Animator anim)
        {
            anim.Play(animationSelected);
        }
    }
}
