using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObserver
{
    void OnAnimationSelected(int animIndex);
}
