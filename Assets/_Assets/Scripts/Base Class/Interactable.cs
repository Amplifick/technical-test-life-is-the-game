using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class Interactable : MonoBehaviour
    {
        public virtual void OnInteract()
        {

        }

        public virtual void OnInteract(PlayerController playerController)
        {

        }
    }
}