using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class UICharacter : MonoBehaviour
    {
        private Animator _animator;
        private PlayerData _playerData;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _playerData = FindObjectOfType<PlayerData>();
        }

        private void Start()
        {
            _playerData.LoadAnimationData(_animator);
        }
    }
}
