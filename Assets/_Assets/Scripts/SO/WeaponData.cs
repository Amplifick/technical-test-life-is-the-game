using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    [CreateAssetMenu(menuName = "Weapon/WeaponData")]
    public class WeaponData : ScriptableObject
    {
        public Weapon weaponPrefab;
        public Bullet gunBullet;

        public float projectileSpeed;
        public float horizontalForce;
        public float verticalForce;
        public float frequency;
        public float amplitude;
        public float attractDistance;
    }
}
