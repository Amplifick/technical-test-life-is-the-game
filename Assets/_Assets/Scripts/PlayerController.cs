using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class PlayerController : MonoBehaviour
    {
        [Header("Reference")]
        private InputManager _inputManager;

        [Header("Interaction Settings")]
        private Transform _camera;
        [SerializeField] float _rayDistance;

        [Header("Weapon Settings")]
        public Transform weaponHolderSlot;
        public Weapon currentWeapon;

        #region Unity Functions
        private void Awake()
        {
            _camera = Camera.main.transform;
            _inputManager = GetComponent<InputManager>();
        }

        private void Update()
        {
            Interaction();
            Shoot();

            DebugRay();
        }
        #endregion

        /// <summary>
        /// Makes the player possible to interact with objects that has the interactable layer mask
        /// when the correct input is triggered.
        /// </summary>
        private void Interaction()
        {
            if (_inputManager.interactInput)
            {
                RaycastHit Hit;

                if (Physics.Raycast(_camera.position, _camera.forward, out Hit, _rayDistance, LayerMask.GetMask("Interactable")))
                {
                    LoadNewWeapon(Hit.transform.GetComponent<Weapon>());
                }
            }

        }

        /// <summary>
        /// Instantiates the model of the weapon in the weapon holder slot.
        /// Sets the new weapon as the current weapon.
        /// </summary>
        /// <param name="weapon"></param>
        private void LoadNewWeapon(Weapon weapon)
        {
            if(weaponHolderSlot.childCount > 0)
            {
                foreach (Transform child in weaponHolderSlot.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            Instantiate(weapon.weaponData.weaponPrefab,weaponHolderSlot);
            currentWeapon = weapon.weaponData.weaponPrefab;
                
        }

        /// <summary>
        /// When the input is executed it shoots a projectile depending of the current weapon.
        /// </summary>
        private void Shoot()
        {
            if (_inputManager.shootInput && currentWeapon)
            {
                currentWeapon.OnShoot();
            }
        }

        #region Debug Functions

        /// <summary>
        /// Shows a ray in the editor for debugging purposes.
        /// </summary>
        private void DebugRay()
        {
            Debug.DrawRay(_camera.position, _camera.forward * _rayDistance, Color.red);
        }

        #endregion
    }
}