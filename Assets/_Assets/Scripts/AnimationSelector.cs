using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class AnimationSelector : MonoBehaviour, IObserver
    {
        [SerializeField] private string[] _animClips;
        private Animator _animator;
        private PlayerData _playerData;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _playerData = FindObjectOfType<PlayerData>();
        }

        /// <summary>
        /// Plays an animation when its triggered from the UI button.
        /// </summary>
        /// <param name="animIndex">Indicates the index of the animation that will be played</param>
        public void OnAnimationSelected(int animIndex)
        {
            _animator.Play(_animClips[animIndex]);
            _playerData.SaveAnimationData(_animClips[animIndex]);
        }

    }
}