using UnityEngine;

namespace DG
{
    public class ParabolicBullet : Bullet
    {
        protected override void Start()
        {
            base.Start();
            rb.AddForce(direction * horizontalForce + Vector3.up * verticalForce);
        }

        private void Update()
        {
            transform.rotation = Quaternion.LookRotation(rb.velocity);  
        }
    }
}
