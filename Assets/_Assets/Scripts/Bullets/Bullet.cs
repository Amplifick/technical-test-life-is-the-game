using UnityEngine;

namespace DG
{
    public class Bullet : MonoBehaviour
    {

        [Header("References")]
        protected Rigidbody rb;

        [Header("Bullet Settings")]
        public Transform bulletSpawnPoint;
        public float projectileSpeed;
        public float horizontalForce;
        public float verticalForce;
        public float frequency;
        public float amplitude;
        public float attractDistance;
        protected Vector3 direction;

      
        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        /// <summary>
        /// Calculates the direction that all the bullets should have by creating a ray 
        /// that is casted from the maincamera according the coordinates of the mouse position.
        /// </summary>
        protected virtual void Start()
        {
            Vector3 ClickPos = Input.mousePosition;

            Ray ray = Camera.main.ScreenPointToRay(ClickPos);

            if(Physics.Raycast(ray, out RaycastHit hitInfo))
            {
                direction = hitInfo.point - bulletSpawnPoint.position;
            }

            Destroy(gameObject,10f);
        }
    }
}