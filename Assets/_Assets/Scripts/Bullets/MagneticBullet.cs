using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class MagneticBullet : Bullet
    {
        
        [SerializeField] private List<GameObject> attractedObjects = new();

        #region Unity Functions
        protected override void Start()
        {
            base.Start();
            rb.AddForce(direction * horizontalForce + Vector3.up * verticalForce);
        }

        private void FixedUpdate()
        {
            MakeObjectsOrbit();            
        }

        /// <summary>
        /// When an object that can be attracted enters the boxcollider of the projectile, it is added to the attracted list.
        /// </summary>
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("CanBeAttracted"))
            {
                other.GetComponent<Rigidbody>().useGravity = false;
                attractedObjects.Add(other.gameObject);
            }
        }

        /// <summary>
        /// When an object that can be attracted exits the boxcollider of the projectile, it is removed from the attracted list.
        /// </summary>
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("CanBeAttracted"))
            {
                other.GetComponent<Rigidbody>().useGravity = true;
                attractedObjects.Remove(other.gameObject);
            }
        }

        #endregion

        /// <summary>
        /// This function makes the objects in the attracted list rotate around the bullet in the 3 axis, x, y and z.
        /// It also applies a force to the same objects making them be attracted to the bullet.This force is calculated
        /// using the Law of Gravitation.
        /// </summary>
        private void MakeObjectsOrbit()
        {
            foreach (GameObject attractedObject in attractedObjects)
            {
                attractedObject.transform.RotateAround(transform.position, Vector3.up, 50.0f * Time.deltaTime);
                attractedObject.transform.RotateAround(transform.position, Vector3.right, 50.0f * Time.deltaTime);
                attractedObject.transform.RotateAround(transform.position, Vector3.forward, 50.0f * Time.deltaTime);

                //If the object is not close enough it will be attracted
                if(Vector3.Distance(attractedObject.transform.position, transform.position) > attractDistance)
                {
                    Vector3 AttractedDirection = (transform.position - attractedObject.transform.position).normalized;
                    float Distance = Vector3.Distance(transform.position, attractedObject.transform.position);
                    float ForceMagnitude = (9 * rb.mass * attractedObject.GetComponent<Rigidbody>().mass) / (Distance * Distance);

                    Vector3 Force = AttractedDirection * ForceMagnitude;
                    attractedObject.GetComponent<Rigidbody>().AddForce(Force);
                }
            }
        }

        private void OnDestroy()
        {
            foreach (GameObject item in attractedObjects)
            {
                item.GetComponent<Rigidbody>().useGravity = true;
            }
        }

    }
}