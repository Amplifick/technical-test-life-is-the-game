using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class SinBullet : Bullet
    {
        protected override void Start()
        {
            base.Start();
            rb.AddForce(Camera.main.transform.forward * projectileSpeed, ForceMode.Impulse);
        }

        private void FixedUpdate()
        {
            // Calculates the sinusoidal force
            verticalForce = Mathf.Sin(Time.time * frequency) * amplitude;

            // Applies the force upwards as a function of the sinusoidal force
            rb.AddForce(Vector3.up * verticalForce, ForceMode.Force);
    
        }
    }
}
