using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class MouseLook : MonoBehaviour
    {
        [Header("References")]
        private InputManager _inputManager;

        [Header("Mouse Setting's")]
        [SerializeField] private float _mouseSensitivity = -100f;
        private float _xRotation = 0f;

        public Transform lookTarget;

        #region Unity Functions

        private void Awake()
        {
            _inputManager = GetComponentInParent<InputManager>();
        }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }


        private void Update()
        {
            Look();
        }

        #endregion

        /// <summary>
        /// Rotates the camera to the desire mouse position.
        /// </summary>
        private void Look()
        {
            float MouseX = _inputManager.mouseLook.x * _mouseSensitivity * Time.deltaTime;
            float MouseY = _inputManager.mouseLook.y * _mouseSensitivity * Time.deltaTime;

            _xRotation -= MouseY;
            _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(_xRotation, 0, 0);
            lookTarget.Rotate(Vector3.up * MouseX);
        }
    }
}