using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class Weapon : MonoBehaviour
    {
        [Header("Weapon Data")]
        public WeaponData weaponData;

        [Header("Weapon Setting's")]
        [SerializeField] private Transform _bulletStartPoint;

        /// <summary>
        /// Instantiates the bullet and loads all the values of it from the scriptable object assigned to the weapon.
        /// </summary>
        public void OnShoot()
        {
            Bullet bullet = Instantiate(weaponData.gunBullet, _bulletStartPoint.position, 
                Quaternion.Euler(_bulletStartPoint.rotation.eulerAngles));
            bullet.bulletSpawnPoint = _bulletStartPoint;
            bullet.horizontalForce = weaponData.horizontalForce;
            bullet.verticalForce = weaponData.verticalForce;
            bullet.projectileSpeed = weaponData.projectileSpeed;
            bullet.frequency = weaponData.frequency;
            bullet.amplitude = weaponData.amplitude;
            bullet.attractDistance = weaponData.attractDistance;
        }
    }
}
