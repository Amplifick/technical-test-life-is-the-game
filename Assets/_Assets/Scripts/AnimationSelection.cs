using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG
{
    public class AnimationSelection : MonoBehaviour
    {
        private List<IObserver> _observers = new();

        private void OnEnable()
        {
            AnimationSelector animationSelector = FindObjectOfType<AnimationSelector>();

            if(animationSelector != null)
            {
                AddObserver(animationSelector);
            }
        }

        private void OnDisable()
        {
            RemoveAllObservers();
        }

        public void AddObserver(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void RemoveAllObservers()
        {
            _observers.Clear();
        }

        /// <summary>
        /// Notifies all the observers attached to this subjects about the new animation selected.
        /// </summary>
        /// <param name="animIndex">The index of the new animation that will be played</param>
        public void NotifyObservers(int animIndex)
        {
            foreach (IObserver observer in _observers)
            {
                observer.OnAnimationSelected(animIndex);
            }
        }
    }
}
